from pymote.algorithm import NodeAlgorithm
from pymote.message import Message

class DFT (NodeAlgorithm):
    required_params = ('informationKey',)
    default_params = {'neighborsKey' : 'Neighbors',}
    
    def visit(self,node, message):
        print "Visit mee!!!"
        #if self.unvisited:
        if node.memory['vis']:
            #next = self.unvisited.pop()
            next = node.memory['vis'].pop()
            '''for x in self.unvisited:
                print x
            '''
            #next = node.memory[self.neighborsKey].pop()
            #print "MESSAGE DATA" , message.data
            node.send(Message(destination=next, header='Token',data=node.memory[self.informationKey]))
            node.status = 'VISITED'
        else :
            if node.memory['init']!= True :
            #if node.status != 'INITIATOR':
                node.send(Message(destination=message.source, header='Return',data=""))
            node.status = 'DONE'

    def initializer (self):
        print "...Initializer...."
        #Fill for each node its, neighbors and set status to idle or initiator#
        firstNodes = []
        for node in self.network.nodes():
            node.memory[self.neighborsKey] = node.compositeSensor.read()['Neighbors']
            #node.memory[self.visn] = node.memory[self.neighborsKey]
            node.memory['vis'] = node.memory[self.neighborsKey]
            node.status = 'IDLE'
            if node.memory.has_key(self.informationKey):
                node.status = 'INITIATOR'
                firstNodes.append(node)
        for fn in firstNodes:
            self.network.outbox.insert(0,Message(header=NodeAlgorithm.INI,
                                              destination=fn))
    #INITIATOR
    def initiator (self, node, message):
        print "___Initiator___"
        if message.header == NodeAlgorithm.INI:
            #self.unvisited = list(node.memory[self.neighborsKey])
            node.memory['vis'] = list(node.memory[self.neighborsKey])
            #self.initiator = True
            node.memory['init']= True
            self.visit(node, message)
    
    #IDLE
    def idle (self, node, message):
        print "___Idle___"  
        if message.header == "Token":
            #self.entry = message.source;
            node.memory[self.informationKey]=message.data
            #self.unvisited=list(node.memory[self.neighborsKey])
            node.memory['vis']=list(node.memory[self.neighborsKey])
            '''for x in self.unvisited:
                print x
            '''
            #self.unvisited.remove(message.source)
            node.memory['vis'].remove(message.source)
            #node.memory[self.visitedNeighbors].remove(message.source)
            #self.initiator = False
            node.memory['init']= False
            self.visit(node, message)
    
    #VISITED
    def visited (self, node, message):
        print "___Visited___"
        if message.header == "Token":
            print "SENDER...", message.source
            '''for x in self.unvisited:
                print x
            '''
            #self.unvisited.remove(message.source)
            node.memory['vis'].remove(message.source)
            
            #node.memory[self.visitedNeighbors].remove(message.source)
            node.send(Message(destination = message.source, header='Backedge',data=""))
        else :
            if message.header == "Return":
                self.visit(node, message)
            else :
                if message.header == "Backedge":
                    self.visit(node, message)
    
    #DONE
    def done (self, node, message):
        print "___Done___"

    STATUS = {
            'INITIATOR' : initiator,
            'IDLE' : idle,
            'VISITED' : visited,
            'DONE' : done,
        }
