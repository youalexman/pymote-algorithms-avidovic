from pymote.algorithm import NodeAlgorithm
from pymote.message import Message

class DFT (NodeAlgorithm):
    required_params = ('informationKey',)
    default_params = {'neighborsKey' : 'Neighbors',}
    
    def visit(self,node, message):
        print "Visit precedure!!!"
        if node.memory['visKey']:
            next = node.memory['visKey'].pop()
            node.send(Message(destination=next, header='Token',data=node.memory[self.informationKey]))
            node.status = 'VISITED'
        else :
            if node.memory['init']!= True :
                node.send(Message(destination=node.memory['senderKey'], header='Return',data=""))
            node.status = 'DONE'

    def initializer (self):
        print "...Initializer...."
        firstNodes = []
        for node in self.network.nodes():
            node.memory[self.neighborsKey] = node.compositeSensor.read()['Neighbors']
            node.status = 'IDLE'
            if node.memory.has_key(self.informationKey):
                node.status = 'INITIATOR'
                firstNodes.append(node)
        for fn in firstNodes:
            self.network.outbox.insert(0,Message(header=NodeAlgorithm.INI,
                                              destination=fn))
    #INITIATOR
    def initiator (self, node, message):
        print "___Initiator___"
        if message.header == NodeAlgorithm.INI:
            node.memory['visKey'] = list(node.memory[self.neighborsKey])
            node.memory['init']= True
            self.visit(node, message)
    
    #IDLE
    def idle (self, node, message):
        print "___Idle___"  
        if message.header == "Token":
            node.memory[self.informationKey]=message.data
            node.memory['visKey']=list(node.memory[self.neighborsKey])
            node.memory['senderKey']=message.source
            node.memory['visKey'].remove(node.memory['senderKey'])
            node.memory['init']= False
            self.visit(node, message)
    
    #VISITED
    def visited (self, node, message):
        print "___Visited___"
        if message.header == "Token":
            node.memory['visKey'].remove(message.source)
            node.send(Message(destination = message.source, header='Backedge',data=""))
        
        elif message.header == "Return" :
            self.visit(node, message)
        
        elif message.header == "Backedge" :
            self.visit(node, message)
    
    #DONE
    def done (self, node, message):
        print "___Done___"

    STATUS = {
            'INITIATOR' : initiator,
            'IDLE' : idle,
            'VISITED' : visited,
            'DONE' : done,
        }
